Source: kgb-bot
Maintainer: Debian KGB Maintainers <kgb-bot@packages.debian.org>
Uploaders: Martina Ferrari <tina@debian.org>,
           Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: git <!nocheck>,
                     libclass-accessor-perl <!nocheck>,
                     libcpan-changes-perl <!nocheck>,
                     libdbd-pg-perl <!nocheck>,
                     libdpkg-perl <!nocheck>,
                     libfile-remove-perl <!nocheck>,
                     libfile-touch-perl <!nocheck>,
                     libipc-run-perl <!nocheck>,
                     libipc-system-simple-perl <!nocheck>,
                     libjson-rpc-perl <!nocheck>,
                     libjson-xs-perl <!nocheck>,
                     liblist-moreutils-perl <!nocheck>,
                     libmonkey-patch-perl <!nocheck>,
                     libnet-ip-perl <!nocheck>,
                     libpoe-component-irc-perl <!nocheck>,
                     libpoe-component-server-soap-perl <!nocheck>,
                     libpoe-perl <!nocheck>,
                     libproc-pid-file-perl <!nocheck>,
                     libschedule-ratelimiter-perl <!nocheck>,
                     libsoap-lite-perl <!nocheck>,
                     libsvn-perl <!nocheck>,
                     libtest-compile-perl <!nocheck>,
                     libtest-differences-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtext-glob-perl <!nocheck>,
                     libwww-shorten-perl <!nocheck>,
                     libyaml-perl <!nocheck>,
                     locales-all,
                     perl,
                     subversion <!nocheck>
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/kgb-team/kgb/
Vcs-Git: https://salsa.debian.org/kgb-team/kgb.git
Homepage: https://salsa.debian.org/kgb-team/kgb/wikis/home
Rules-Requires-Root: no

Package: kgb-bot
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         adduser,
         git,
         kgb-client (= ${binary:Version}),
         libipc-run-perl,
         libjson-xs-perl,
         liblist-moreutils-perl,
         libmonkey-patch-perl,
         libnet-ip-perl,
         libpoe-component-irc-perl,
         libpoe-component-server-soap-perl,
         libpoe-component-sslify-perl,
         libpoe-perl,
         libproc-pid-file-perl,
         libschedule-ratelimiter-perl,
         libtext-glob-perl,
         libyaml-perl,
         lsb-base
Suggests: libfile-which-perl,
          polygen
Pre-Depends: ${misc:Pre-Depends}
Description: IRC collaboration bot
 KGB is an IRC bot, helping people work together by notifying an IRC channel
 when a commit occurs.
 .
 It supports multiple repositories/IRC channels and is fully configurable.
 .
 This package contains the server-side daemon, kgb-bot, which is responsible
 for relaying commit notifications to IRC.

Package: kgb-client
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-accessor-perl,
         libjson-rpc-perl,
         libjson-xs-perl,
         libsoap-lite-perl,
         libsvn-perl,
         libyaml-perl
Recommends: libdpkg-perl,
            libfile-touch-perl,
            libwww-perl,
            libwww-shorten-perl
Suggests: kgb-bot
Description: client for KGB (IRC collaboration bot)
 KGB is an IRC bot, helping people work together by notifying an IRC channel
 when a commit occurs.
 .
 It supports multiple repositories/IRC channels and is fully configurable.
 .
 This package contains the client-side program, kgb-client, which is supposed
 to be used as an hook in your version control system and sends the
 notifications to the KGB daemon.
 .
 Currently supported version control systems are:
  * Git
  * Subversion
  * CVS
